# Flight Passengers (Operations/Main) - Spring Boot

This is a simple flight/passengers REST API made with Spring Boot for the following operations:

* create a new flight
* get flight passengers by flight number
* add passengers to a flight

There is no authentication implemented.  
Below you can find some details and instructions on how to use it.

**LIMITATIONS**  
1. no seat number validation (it accepts any string)  
2. no validation of flight capacity  
3. no origin/destination/departure time, so all flights are unique just for testing purposes  

**VALIDATIONS**  
1: when adding a new passenger, if that seat is already taken for that particular flight  
2: when adding a new passenger, if the passenger is already in that flight (by firstname + lastname + flightNumber)  
3: when searching a flight by number, we validate if the flight exists  
4: when creating a new flight it should have a unique number

**TODOS**  
1: docker??  

**NOTES**  
1: no docker so far  


## Start Flight Passengers Operation (Main) Spring Boot
Requirements:

* [git](https://git-scm.com/downloads)
* [maven](https://maven.apache.org/download.cgi)
* [Java 8+](https://www.java.com/download/) installed in your environment
* have mvn executable set in your PATH

Steps:

1. Clone this bitbucket repo in your local environment onto a folder of your choice executing the following:  
 ```
 git clone https://paulobing@bitbucket.org/paulobing/passengers-operations.git
 ```
2. To start springboot execute from inside the downloaded directory with the project (it might take a few minutes the first time to download dependencies):
 ```
 mvn spring-boot:run
 ```
3. Choose one of below endpoints or swagger-ui to test the endpoints.

Tests:

There are some test cases and you may execute the tests or clean install to check full compile and tests execution (including rest-assured endpoint tests):  
 ```
 mvn clean install
 ```
 ```
 mvn test
 ```


## Swagger API Interface
To see the API in an interactive way, you can access below url, which will redirect to swagger-ui page:  
```
http://localhost:8080/
```

# API Endpoints
As mentioned above, below is a list of endpoints available to perform each action with an example:

## CreateFlight (POST)
To create a new flight:
```java
POST localhost:8080/manifest/createFlight/{flightNumber}
```
Example:
```
curl --request POST http://localhost:8080/manifest/createFlight/VL1234
```

## GetManifest (GET)
To get the flight and passengers using flightNumber as a parameter:
```java
GET localhost:8080/manifest/{flightNumber}
```
Example:
```java
curl --request GET http://localhost:8080/manifest/VL1234
```

## AddPassenger (POST)
To add a passenger (in request body) to an already existing flight:
```java
POST localhost:8080/manifest/add/{flightNumber}
```
Example:
```
curl --request POST -H "Content-Type: application/json" --data '{ "name": "Arnold", "lastname": "Schwarzenegger", "seat": "1A" }' http://localhost:8080/manifest/add/VL1234
```
