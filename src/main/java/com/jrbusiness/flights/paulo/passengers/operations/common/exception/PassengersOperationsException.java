package com.jrbusiness.flights.paulo.passengers.operations.common.exception;

public abstract class PassengersOperationsException extends Exception {
    public PassengersOperationsException(String message) {
        super(message);
    }
}
