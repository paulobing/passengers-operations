package com.jrbusiness.flights.paulo.passengers.operations.flight.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jrbusiness.flights.paulo.passengers.operations.common.exception.FlightAlreadyExistsException;
import com.jrbusiness.flights.paulo.passengers.operations.common.exception.FlightNotFoundException;
import com.jrbusiness.flights.paulo.passengers.operations.common.exception.PassengerAlreadyInFlightException;
import com.jrbusiness.flights.paulo.passengers.operations.common.exception.SeatAlreadyTakenException;
import com.jrbusiness.flights.paulo.passengers.operations.flight.dto.FlightDTO;
import com.jrbusiness.flights.paulo.passengers.operations.flight.dto.PassengerDTO;
import com.jrbusiness.flights.paulo.passengers.operations.flight.model.Flight;
import com.jrbusiness.flights.paulo.passengers.operations.flight.model.Passenger;
import com.jrbusiness.flights.paulo.passengers.operations.flight.repository.FlightRepository;
import com.jrbusiness.flights.paulo.passengers.operations.flight.repository.PassengerRepository;

@Service
public class FlightPassengerServiceImpl implements FlightPassengerService {
    private FlightRepository flightRepository;
    private PassengerRepository passengerRepository;
    private ModelMapper modelMapper;

    @Autowired
    public FlightPassengerServiceImpl(FlightRepository flightRepository, PassengerRepository passengerRepository, ModelMapper modelMapper) {
        this.flightRepository = flightRepository;
        this.passengerRepository = passengerRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public FlightDTO createFlight(String flightNumber) throws FlightAlreadyExistsException {
        if (doesFlightAlreadyExist(flightNumber)) {
            throw new FlightAlreadyExistsException(String.format("Cannot create flight %s because it already exists!", flightNumber));
        }
        return convert(flightRepository.save(new Flight(flightNumber)));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public FlightDTO getFlightByFlightNumber(String flightNumber) throws FlightNotFoundException {
        return convert(getFlightIfFoundOrThrowError(flightNumber));
    }
    
    @Override
    @Transactional(rollbackFor = Exception.class)
    public PassengerDTO addPassengerToFlight(PassengerDTO passengerDTO, String flightNumber)
            throws FlightNotFoundException, SeatAlreadyTakenException, PassengerAlreadyInFlightException {
        Flight flight = getFlightIfFoundOrThrowError(flightNumber);
        validateIfSeatAlreadyTakenInFlight(flight, passengerDTO.getSeat());
        validateIfPassengerAlreadyInFlight(flight, passengerDTO);
        Passenger newPassenger = convert(passengerDTO);
        newPassenger.setFlight(flight);
        passengerRepository.save(newPassenger);
        
        return convert(newPassenger);
    }

    private void validateIfSeatAlreadyTakenInFlight(Flight flight, String seat) throws SeatAlreadyTakenException {
        if (flight.getPassengers().stream().anyMatch(passenger -> seat.equals(passenger.getSeat()))) {
            throw new SeatAlreadyTakenException(String.format("Seat %s is already taken on flight %s!", seat, flight.getFlightNumber()));
        }
    }
    
    private void validateIfPassengerAlreadyInFlight(Flight flight, PassengerDTO passengerDTO) throws PassengerAlreadyInFlightException {
        if (flight.getPassengers().stream().anyMatch(passenger -> passengerDTO.getLastname().equals(passenger.getLastname()) && passengerDTO.getName().equals(passenger.getName()))) {
            final String errorMessage = String.format(
                    "Passenger %s %s is already in flight %s!",
                    passengerDTO.getName(),
                    passengerDTO.getLastname(),
                    flight.getFlightNumber());
            throw new PassengerAlreadyInFlightException(errorMessage);
        }
    }

    private Flight getFlightIfFoundOrThrowError(String flightNumber) throws FlightNotFoundException {
        return flightRepository.findByFlightNumber(flightNumber).orElseThrow(() -> new FlightNotFoundException(String.format("Flight %s not found!", flightNumber)));
    }

    private boolean doesFlightAlreadyExist(String flightNumber) {
        return flightRepository.findByFlightNumber(flightNumber).isPresent();
    }

    private FlightDTO convert(Flight flight) {
        FlightDTO flightDTO = modelMapper.typeMap(Flight.class, FlightDTO.class).map(flight);
        flightDTO.setPassengers(convert(flight.getPassengers()));
        return flightDTO;
    }

    private List<PassengerDTO> convert(List<Passenger> passengers) {
        java.lang.reflect.Type targetListType = new TypeToken<List<PassengerDTO>>() {}.getType();
        return modelMapper.map(passengers, targetListType);
    }

    private Passenger convert(PassengerDTO passengerDTO) {
        return modelMapper.typeMap(PassengerDTO.class, Passenger.class)
                .map(passengerDTO);
    }
    
    private PassengerDTO convert(Passenger passenger) {
        return modelMapper.typeMap(Passenger.class, PassengerDTO.class).map(passenger);
    }

}
