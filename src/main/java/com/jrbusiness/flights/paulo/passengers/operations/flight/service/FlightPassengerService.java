package com.jrbusiness.flights.paulo.passengers.operations.flight.service;

import com.jrbusiness.flights.paulo.passengers.operations.common.exception.FlightAlreadyExistsException;
import com.jrbusiness.flights.paulo.passengers.operations.common.exception.FlightNotFoundException;
import com.jrbusiness.flights.paulo.passengers.operations.common.exception.PassengerAlreadyInFlightException;
import com.jrbusiness.flights.paulo.passengers.operations.common.exception.SeatAlreadyTakenException;
import com.jrbusiness.flights.paulo.passengers.operations.flight.dto.FlightDTO;
import com.jrbusiness.flights.paulo.passengers.operations.flight.dto.PassengerDTO;

public interface FlightPassengerService {
    FlightDTO createFlight(String flightNumber) throws FlightAlreadyExistsException;
    FlightDTO getFlightByFlightNumber(String flightNumber) throws FlightNotFoundException;
    PassengerDTO addPassengerToFlight(PassengerDTO passengerDTO, String flightNumber) throws FlightNotFoundException, SeatAlreadyTakenException, PassengerAlreadyInFlightException;
}
