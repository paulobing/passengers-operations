package com.jrbusiness.flights.paulo.passengers.operations.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Flight with this flight number already exists.")
public class FlightNotFoundException extends PassengersOperationsException {

    public FlightNotFoundException(String message) {
        super(message);
    }
}
