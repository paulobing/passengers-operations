package com.jrbusiness.flights.paulo.passengers.operations.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Flight with this flight number was not found.")
public class FlightAlreadyExistsException extends PassengersOperationsException {

    public FlightAlreadyExistsException(String message) {
        super(message);
    }
}
