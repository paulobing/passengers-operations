package com.jrbusiness.flights.paulo.passengers.operations.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Seat already taken in this flight.")
public class SeatAlreadyTakenException extends PassengersOperationsException {

    public SeatAlreadyTakenException(String message) {
        super(message);
    }
}
