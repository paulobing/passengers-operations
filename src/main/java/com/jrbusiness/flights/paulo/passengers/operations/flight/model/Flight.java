package com.jrbusiness.flights.paulo.passengers.operations.flight.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@SequenceGenerator(name="seq_flight", initialValue=1, allocationSize=100)
@Getter
@Setter
@Table(name="flight")
@NoArgsConstructor(access=AccessLevel.PUBLIC)
@AllArgsConstructor(access=AccessLevel.PUBLIC)
public class Flight {
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_flight")
    private Long id;
    
    @Column(nullable=false)
    @NotNull
    private String flightNumber;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER,
            mappedBy = "flight",
            orphanRemoval = true)
    @OrderBy("id")
    private List<Passenger> passengers = new ArrayList<>();
    
    public Flight(String flightNumber) {
        this.flightNumber = flightNumber;
    }
}
