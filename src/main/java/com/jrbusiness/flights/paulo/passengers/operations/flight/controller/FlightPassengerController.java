package com.jrbusiness.flights.paulo.passengers.operations.flight.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jrbusiness.flights.paulo.passengers.operations.common.exception.FlightAlreadyExistsException;
import com.jrbusiness.flights.paulo.passengers.operations.common.exception.FlightNotFoundException;
import com.jrbusiness.flights.paulo.passengers.operations.common.exception.PassengerAlreadyInFlightException;
import com.jrbusiness.flights.paulo.passengers.operations.common.exception.SeatAlreadyTakenException;
import com.jrbusiness.flights.paulo.passengers.operations.flight.dto.FlightDTO;
import com.jrbusiness.flights.paulo.passengers.operations.flight.dto.PassengerDTO;
import com.jrbusiness.flights.paulo.passengers.operations.flight.service.FlightPassengerService;

@RestController
@RequestMapping("manifest")
public class FlightPassengerController {
    private FlightPassengerService flightPassengerService;

    @Autowired
    public FlightPassengerController(FlightPassengerService flightPassengerService) {
        this.flightPassengerService = flightPassengerService;
    }
    
    @PostMapping("/createFlight/{flightNumber}")
    public ResponseEntity<FlightDTO> createFlight(@PathVariable String flightNumber) throws FlightAlreadyExistsException {
        return new ResponseEntity<>(flightPassengerService.createFlight(flightNumber), HttpStatus.OK);
    }
    
    @GetMapping("/{flightNumber}")
    public ResponseEntity<FlightDTO> getFlightByFlightNumber(@PathVariable String flightNumber) throws FlightNotFoundException {
        return new ResponseEntity<>(flightPassengerService.getFlightByFlightNumber(flightNumber), HttpStatus.OK);
    }

    @PostMapping("/add/{flightNumber}")
    public ResponseEntity<PassengerDTO> addPassengerToFlight(
            @RequestBody PassengerDTO passengerDTO,
            @PathVariable String flightNumber)
    throws FlightNotFoundException, SeatAlreadyTakenException, PassengerAlreadyInFlightException {
        return new ResponseEntity<>(flightPassengerService.addPassengerToFlight(passengerDTO, flightNumber), HttpStatus.OK);
    }

}
