package com.jrbusiness.flights.paulo.passengers.operations.flight.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.jrbusiness.flights.paulo.passengers.operations.flight.model.Passenger;

@Repository
public interface PassengerRepository extends CrudRepository<Passenger, Long> {
}
