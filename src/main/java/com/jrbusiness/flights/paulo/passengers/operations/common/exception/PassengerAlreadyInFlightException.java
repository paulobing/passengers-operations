package com.jrbusiness.flights.paulo.passengers.operations.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "This passenger is already in this flight.")
public class PassengerAlreadyInFlightException extends PassengersOperationsException {

    public PassengerAlreadyInFlightException(String message) {
        super(message);
    }
}
