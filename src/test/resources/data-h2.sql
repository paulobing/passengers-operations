insert into flight (id, flight_number)
values (seq_flight.nextval, 'VL1234');

insert into flight_passenger (id, name, lastname, seat, flight_id)
select seq_passenger.nextval, 'Paulo', 'Bing', '13B', id from flight where flight_number = 'VL1234';
insert into flight_passenger (id, name, lastname, seat, flight_id)
select seq_passenger.nextval, 'Sofia', 'Bing', '13A', id from flight where flight_number = 'VL1234';
