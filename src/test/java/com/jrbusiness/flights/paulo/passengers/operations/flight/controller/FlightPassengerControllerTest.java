package com.jrbusiness.flights.paulo.passengers.operations.flight.controller;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.jrbusiness.flights.paulo.passengers.operations.flight.dto.PassengerDTO;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.DEFINED_PORT)
@TestPropertySource(properties = "server.port=8080")
public class FlightPassengerControllerTest {
    private static final String URL_GET_FLIGHT_AND_PASSENGERS_DETAILS = "/manifest/{flightNumber}";
    private static final String URL_POST_ADD_PASSENGER_TO_FLIGHT = "/manifest/add/{flightNumber}";
    private static final String URL_POST_CREATE_FLIGHT = "/manifest/createFlight/{flightNumber}";
    private static final String FLIGHT_NUMBER = "VL1234";
    private static final String NEW_FLIGHT_NUMBER = "VL9999";
    
    @Before
    public void before() {
    }

    @Test
    public void whenCreateFlight_thenFlightNumberMatches() {
        final String flightNumber = NEW_FLIGHT_NUMBER;
        
        Response responseCreateFlight = getCreateFlightResponse(flightNumber);
        responseCreateFlight.prettyPrint();
        responseCreateFlight
            .then()
            .assertThat()
            .statusCode(200)
            .body("flight", equalTo(flightNumber))
            .body("passengers", Matchers.emptyCollectionOf(PassengerDTO.class));
    }

    @Test
    public void whenCreateAlreadyExistingFlight_thenErrorMessageIsExpected() {
        final String flightNumber = FLIGHT_NUMBER;
        
        Response responseCreateFlight = getCreateFlightResponse(flightNumber);
        responseCreateFlight.prettyPrint();
        responseCreateFlight
            .then()
            .assertThat()
            .statusCode(400)
            .body("message", equalTo(String.format(
                    "Cannot create flight %s because it already exists!", flightNumber)));
    }

    @Test
    public void whenSearchedPassengerIsInFlight_thenNamesAndSeatMatch() {
        final String flightNumber = FLIGHT_NUMBER;
        
        Response responseGetFlight = getFlightAndPassengersResponse(flightNumber);
        responseGetFlight.prettyPrint();
        responseGetFlight
            .then()
            .assertThat()
            .statusCode(200)
            .body("flight", equalTo(flightNumber))
            .body("passengers[0].name", equalTo("Paulo"))
            .body("passengers[0].lastname", equalTo("Bing"))
            .body("passengers[0].seat", equalTo("13B"))
            .body("passengers[1].name", equalTo("Sofia"))
            .body("passengers[1].lastname", equalTo("Bing"))
            .body("passengers[1].seat", equalTo("13A"));
    }
    
    @Test
    public void whenAddPassenger_thenGetPassengersShouldHaveNewPassenger() {
        final String flightNumber = FLIGHT_NUMBER;
        
        PassengerDTO passengerDTO = buildPassengerDTO("John", "Bing", "25F");
        Response responseAddPassenger = getAddPassengerToFlightResponse(flightNumber, passengerDTO);
        responseAddPassenger.prettyPrint();
        responseAddPassenger
            .then()
            .assertThat()
            .statusCode(200)
            .body("name", equalTo(passengerDTO.getName()))
            .body("lastname", equalTo(passengerDTO.getLastname()))
            .body("seat", equalTo(passengerDTO.getSeat()));
            
        Response responseFlight = getFlightAndPassengersResponse(flightNumber);
        responseFlight.prettyPrint();
        responseFlight
                .then()
                .assertThat()
                .statusCode(200)
                .body("flight", equalTo(flightNumber))
                .body("passengers[2].name", equalTo(passengerDTO.getName()))
                .body("passengers[2].lastname", equalTo(passengerDTO.getLastname()))
                .body("passengers[2].seat", equalTo(passengerDTO.getSeat()));
    }

    @Test
    public void whenAddAlreadyExistingPassenger_thenErrorMessageIsExpected() {
        final String flightNumber = FLIGHT_NUMBER;
        
        // already added on data-h2.sql with another seat
        PassengerDTO passengerDTO = buildPassengerDTO("Paulo", "Bing", "1A");
        Response responseAddPassenger = getAddPassengerToFlightResponse(flightNumber, passengerDTO);
        responseAddPassenger.prettyPrint();
        responseAddPassenger
            .then()
            .assertThat()
            .statusCode(400)
            .body("message", equalTo(String.format(
                    "Passenger %s %s is already in flight %s!", passengerDTO.getName(), passengerDTO.getLastname(), flightNumber)));
    }

    @Test
    public void whenAddPassengerToAlreadyTakenSeat_thenErrorMessageIsExpected() {
        final String flightNumber = FLIGHT_NUMBER;
        
        // seat already added on data-h2.sql with another passenger
        PassengerDTO passengerDTO = buildPassengerDTO("John", "Smith", "13B");
        Response responseAddPassenger = getAddPassengerToFlightResponse(flightNumber, passengerDTO);
        responseAddPassenger.prettyPrint();
        responseAddPassenger
            .then()
            .assertThat()
            .statusCode(400)
            .body("message", equalTo(String.format("Seat %s is already taken on flight %s!", passengerDTO.getSeat(), flightNumber)));
    }

    private Response getCreateFlightResponse(final String flightNumber) {
        return given()
            .pathParam("flightNumber", flightNumber)
            .when()
            .post(URL_POST_CREATE_FLIGHT);
    }

    private Response getFlightAndPassengersResponse(final String flightNumber) {
        return given()
            .pathParam("flightNumber", flightNumber)
            .when()
            .get(URL_GET_FLIGHT_AND_PASSENGERS_DETAILS);
    }

    private Response getAddPassengerToFlightResponse(final String flightNumber, final PassengerDTO passengerDTO) {
        return given()
            .pathParam("flightNumber", flightNumber)
            .contentType(ContentType.JSON)
            .body(passengerDTO)
            .when()
            .post(URL_POST_ADD_PASSENGER_TO_FLIGHT);
    }

    private PassengerDTO buildPassengerDTO(String name, String lastname, String seat) {
        return new PassengerDTO(name, lastname, seat);
    }

}
